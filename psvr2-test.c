/*
 * Copyright 2023 Jan Schmidt <jan#centricular.com>
 * SPDX-License-Identifier: BSL-1.0
 */
#include <asm/byteorder.h>
#include <errno.h>
#include <hidapi.h>
#include <inttypes.h>
#include <libusb.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define ASSERT_MSG(_v, ...)                                                    \
  if (!(_v)) {                                                                 \
    fprintf(stderr, __VA_ARGS__);                                              \
    exit(1);                                                                   \
  }
#define ASSERT_MSG_RET0(_v, ...)                                               \
  if (!(_v)) {                                                                 \
    fprintf(stderr, __VA_ARGS__);                                              \
    return 0;                                                                  \
  }

#define PSVR2_VID 0x054c
#define PSVR2_PID 0x0cde

const uint64_t TIME_1S_IN_NS = 1000000000ULL;
const uint64_t TIME_1MS_IN_NS = 1000000ULL;

int sleep_ms(int millis) {
  int ret;
  struct timespec sleep_ns;

  sleep_ns.tv_sec = millis / 1000;
  sleep_ns.tv_nsec = (TIME_1MS_IN_NS * millis) % TIME_1S_IN_NS;

  do {
    ret = nanosleep(&sleep_ns, &sleep_ns);
  } while (ret < 0 && errno == -EINTR);

  return ret;
}

uint64_t get_time_ns() {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);

  uint64_t ticks = now.tv_sec * TIME_1S_IN_NS + now.tv_nsec;
  return ticks;
}

static void hexdump_buffer(const char *label, const unsigned char *buf,
                           int length) {
  int indent;
  char ascii[17];

  if (label)
    indent = strlen(label) + 2;
  printf("%s: ", label);

  ascii[16] = '\0';
  for (int i = 0; i < length; i++) {
    printf("%02x ", buf[i]);

    if (buf[i] >= ' ' && buf[i] <= '~')
      ascii[i % 16] = buf[i];
    else
      ascii[i % 16] = '.';

    if ((i % 16) == 15 || (i + 1) == length) {
      if ((i % 16) < 15) {
        int remain = 15 - (i % 16);
        ascii[(i + 1) % 16] = '\0';
        /* Pad the hex dump out to 48 chars */
        printf("%*s", 3 * remain, " ");
      }
      printf("| %s", ascii);

      if ((i + 1) != length)
        printf("\n%*s", indent, " ");
    }
  }
  printf("\n");
}

static int get_report(hid_device *hid, char id, unsigned char *buf,
                      int length) {
  buf[0] = id;
  memset(&buf[1], 0, length - 1);
  return hid_get_feature_report(hid, buf, length);
}

static int wait_for_ack(hid_device *hid, int timeout_ms) {
  unsigned char buf[16];
  int ret = 0;
  uint64_t start = get_time_ns();
  do {
    ret = get_report(hid, 0xf2, buf, sizeof(buf));
    ASSERT_MSG(ret >= 0, "Failed to read HID acknowledgement. ret %d\n", ret);
    ASSERT_MSG(ret >= 0x10, "HID acknowledgement was short. Read %d bytes\n",
               ret);
    if ((buf[3] & 0x01) == 0)
      break;
    if (get_time_ns() - start >= TIME_1MS_IN_NS * timeout_ms) {
      fprintf(stderr, "Reading HID acknowledgement timed out\n");
      return -1;
    }
    /* Sleep 100ms between retries */
    sleep_ms(100);
  } while (true);

  hexdump_buffer("HID acknowledgement", buf, ret);

  return 0;
}

int do_initial_controls(hid_device *psvr2_hid,
                        libusb_device_handle *psvr2_dev) {
  int ret;
  unsigned char buf[256];

  ret =
      libusb_control_transfer(psvr2_dev,
                              LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
                                  LIBUSB_RECIPIENT_ENDPOINT,
                              0x1, 0x81, 0x0, buf, 0x58, 100);
  ASSERT_MSG(ret >= 0, "USB transfer 1 failed. ret %d\n", ret);
  hexdump_buffer("Read Ctrl 1", buf, ret);

  ret =
      libusb_control_transfer(psvr2_dev,
                              LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
                                  LIBUSB_RECIPIENT_ENDPOINT,
                              0x1, 0x8f, 0x0, buf, 0x108, 100);
  ASSERT_MSG(ret >= 0, "USB transfer 2 failed. ret %d\n", ret);
  hexdump_buffer("Read Ctrl 2", buf, ret);

  /* Send initial control pkts */
  unsigned char pkt1[] = {0x11, 0x00, 0x01, 0x00, 0x03, 0x00,
                          0x00, 0x00, 0x01, 0xED, 0x00};
  ret = libusb_control_transfer(
      psvr2_dev, LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_ENDPOINT, 0x9,
      pkt1[0], 0x0, pkt1, sizeof(pkt1), 100);
  ASSERT_MSG(ret >= 0, "USB transfer 3 failed. ret %d\n", ret);

  unsigned char pkt2[] = {0x12, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x1F};
  ret = libusb_control_transfer(
      psvr2_dev, LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_ENDPOINT, 0x9,
      pkt2[0], 0x0, pkt2, sizeof(pkt2), 100);
  ASSERT_MSG(ret >= 0, "USB transfer 4 failed. ret %d\n", ret);

  unsigned char pkt3[] = {0x07, 0x00, 0x01, 0x00, 0x02,
                          0x00, 0x00, 0x00, 0x4E, 0x00};
  ret = libusb_control_transfer(
      psvr2_dev, LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_ENDPOINT, 0x9,
      pkt3[0], 0x0, pkt3, sizeof(pkt3), 100);
  ASSERT_MSG(ret >= 0, "USB transfer 5 failed. ret %d\n", ret);

  unsigned char pkt4[] = {0x0D, 0x00, 0x07, 0x00, 0x0D, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02};
  ret = libusb_control_transfer(
      psvr2_dev, LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_ENDPOINT, 0x9,
      pkt4[0], 0x0, pkt4, sizeof(pkt4), 100);
  ASSERT_MSG(ret >= 0, "USB transfer 6 failed. ret %d\n", ret);

  /* Send initial control pkts */
  ret =
      libusb_control_transfer(psvr2_dev,
                              LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR |
                                  LIBUSB_RECIPIENT_ENDPOINT,
                              0x1, 0x8A, 0x0, buf, 0x10, 100);
  ASSERT_MSG(ret >= 0, "USB transfer 7 failed. ret %d\n", ret);
  hexdump_buffer("Read Ctrl 3", buf, ret);

  unsigned char pkt5[] = {0x0B, 0x00, 0x01, 0x00, 0x08, 0x00, 0x00, 0x00,
                          0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00};
  ret = libusb_control_transfer(
      psvr2_dev, LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_ENDPOINT, 0x9,
      pkt5[0], 0x0, pkt5, sizeof(pkt5), 100);
  ASSERT_MSG(ret >= 0, "USB transfer 8 failed. ret %d\n", ret);

  unsigned char hid_pkt1[] = {
      0xf0, 0x01, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x64, 0x14, 0x84,
      0x42, 0xe5, 0x7c, 0x04, 0x51, 0xcd, 0x34, 0xff, 0x03, 0x3b, 0x92,
      0x37, 0x27, 0x10, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x30, 0x31,
      0x30, 0x30, 0x30, 0x31, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
      0x30, 0xbb, 0x3d, 0x50, 0x84, 0x4a, 0x5d, 0xa7, 0x0a,
  };
  ret = hid_send_feature_report(psvr2_hid, hid_pkt1, sizeof(hid_pkt1));
  ASSERT_MSG(ret >= 0, "USB HID pkt 1 failed. ret %d\n", ret);

  unsigned char hid_pkt2[] = {
      0xf0, 0x01, 0x01, 0x01, 0x36, 0x61, 0xc6, 0x72, 0xb1, 0xe9, 0x22,
      0x44, 0xde, 0x06, 0x75, 0xf8, 0x9b, 0xf0, 0xea, 0x7f, 0x6b, 0x49,
      0xcd, 0x63, 0xd4, 0x2b, 0x25, 0xad, 0xdd, 0x3a, 0x34, 0xeb, 0xf9,
      0x07, 0xb2, 0x0c, 0x46, 0x5d, 0x52, 0xb0, 0x33, 0x5d, 0x11, 0xda,
      0x37, 0x2d, 0x62, 0xc3, 0xcc, 0x44, 0x09, 0x37, 0x35, 0x4e, 0x59,
      0x25, 0x80, 0x2d, 0xc2, 0x7a, 0x0a, 0xd7, 0x6f, 0xc8,
  };
  ret = hid_send_feature_report(psvr2_hid, hid_pkt2, sizeof(hid_pkt2));
  ASSERT_MSG(ret >= 0, "USB HID pkt 2 failed. ret %d\n", ret);

  unsigned char hid_pkt3[] = {
      0xf0, 0x01, 0x01, 0x02, 0xce, 0xa4, 0x0f, 0xd2, 0xa0, 0x11, 0x56,
      0x2c, 0x2b, 0xf2, 0x69, 0x70, 0xde, 0xbd, 0x4a, 0x49, 0xeb, 0xb1,
      0x17, 0xcf, 0x92, 0x2a, 0xec, 0x63, 0xa9, 0x37, 0x77, 0x7a, 0x31,
      0xe1, 0x61, 0x1a, 0x84, 0x48, 0xba, 0x1d, 0x37, 0x05, 0x6a, 0xc2,
      0x61, 0x05, 0x67, 0x85, 0xc6, 0x54, 0x70, 0x22, 0xa8, 0xfe, 0x73,
      0x9b, 0x2e, 0x9b, 0x8e, 0xf1, 0xc1, 0x9b, 0x7b, 0x9d,
  };
  ret = hid_send_feature_report(psvr2_hid, hid_pkt3, sizeof(hid_pkt3));
  ASSERT_MSG(ret >= 0, "USB HID pkt 3 failed. ret %d\n", ret);

  unsigned char hid_pkt4[] = {
      0xf0, 0x01, 0x01, 0x03, 0x43, 0x59, 0x41, 0x74, 0x96, 0x91, 0xec,
      0xea, 0x49, 0xf2, 0x6d, 0xb9, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x5c, 0x86, 0x2a, 0x07,
  };
  ret = hid_send_feature_report(psvr2_hid, hid_pkt4, sizeof(hid_pkt4));
  ASSERT_MSG(ret >= 0, "USB HID pkt 4 failed. ret %d\n", ret);

  wait_for_ack(psvr2_hid, 1000);

  for (int i = 0; i < 4; i++) {
    ret = get_report(psvr2_hid, 0xf1, buf, 64);
    ASSERT_MSG(ret >= 0, "USB HID 0xf1 failed. ret %d i %d\n", ret, i);
    hexdump_buffer("USB HID 0xf1", buf, ret);
  }

  unsigned char hid_pkt5[] = {
      0xf0, 0x02, 0x01, 0x00, 0x63, 0x87, 0x99, 0xdc, 0x13, 0x7b, 0xc1,
      0x49, 0xc3, 0xf1, 0xd9, 0x81, 0x68, 0x8d, 0xab, 0xcc, 0x01, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08,
      0xe6, 0x41, 0x85, 0x37, 0xbc, 0xc8, 0xd2, 0xa7, 0xe9, 0xf9, 0xc3,
      0xb6, 0x80, 0x67, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x1b, 0xad, 0xaf, 0x43,
  };
  ret = hid_send_feature_report(psvr2_hid, hid_pkt5, sizeof(hid_pkt5));
  ASSERT_MSG(ret >= 0, "USB HID pkt 5 failed. ret %d\n", ret);

  wait_for_ack(psvr2_hid, 1000);

  return 0;
}

int send_ep0_08_pkt(libusb_device_handle *psvr2_dev) {
  unsigned char pkt_ep0_08[] = {0x08, 0x00, 0x01, 0x00, 0x01,
                                0x00, 0x00, 0x00, 0x00};

  int ret = libusb_control_transfer(
      psvr2_dev, LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_ENDPOINT, 0x9,
      pkt_ep0_08[0], 0x0, pkt_ep0_08, sizeof(pkt_ep0_08), 100);
  ASSERT_MSG(ret >= 0, "USB 08 pkt failed. ret %d\n", ret);

  return 0;
}

int send_ep0_0a_pkt(libusb_device_handle *psvr2_dev) {
  unsigned char pkt_ep0_0a[] = {
      0x0a, 0x00, 0x01, 0x00, 0xd8, 0x00, 0x00, 0x00, 0xf3, 0x7c, 0x73, 0x09,
      0x01, 0x00, 0x00, 0x00, 0x06, 0x00, 0x68, 0x01, 0xff, 0x7f, 0xf3, 0xbc,
      0xc0, 0x6f, 0xf7, 0xba, 0x64, 0x5c, 0x7c, 0x3d, 0x2c, 0xec, 0x0e, 0x3f,
      0x46, 0xdd, 0x5c, 0x3e, 0x0e, 0x16, 0x4d, 0xbf, 0x12, 0xaf, 0x30, 0xbd,
      0x68, 0xee, 0x74, 0xbf, 0x53, 0x47, 0x93, 0xbe, 0x14, 0x1a, 0x54, 0xbf,
      0xba, 0xd5, 0x47, 0x3e, 0x78, 0x5c, 0x06, 0xbf, 0x25, 0xea, 0xa0, 0x4a,
      0xa1, 0x25, 0xb0, 0xc9, 0xd8, 0x7e, 0x6a, 0xc9, 0xb0, 0x16, 0x65, 0xc7,
      0xe4, 0xc3, 0x78, 0xca, 0x7d, 0x92, 0x29, 0x4a, 0xa1, 0x25, 0xb0, 0xc9,
      0x8e, 0xfa, 0xdc, 0x4a, 0xe8, 0xd2, 0xdb, 0xc9, 0xca, 0x43, 0x8a, 0x4a,
      0x4c, 0xab, 0x40, 0xc9, 0xc8, 0xa2, 0xab, 0xca, 0xd8, 0x7e, 0x6a, 0xc9,
      0xe8, 0xd2, 0xdb, 0xc9, 0x5c, 0x82, 0xad, 0x4a, 0xc3, 0x07, 0x34, 0xca,
      0xb4, 0x4f, 0x92, 0x4a, 0xe8, 0xb4, 0x34, 0x49, 0xb0, 0x16, 0x65, 0xc7,
      0xca, 0x43, 0x8a, 0x4a, 0xc3, 0x07, 0x34, 0xca, 0x97, 0x34, 0x8c, 0x4a,
      0xe1, 0x93, 0xf2, 0xc9, 0x41, 0x8b, 0x20, 0xca, 0xe4, 0xc3, 0x78, 0xca,
      0x4c, 0xab, 0x40, 0xc9, 0xb4, 0x4f, 0x92, 0x4a, 0xe1, 0x93, 0xf2, 0xc9,
      0xa9, 0x10, 0xde, 0x4a, 0x4b, 0x59, 0x10, 0xc9, 0x7d, 0x92, 0x29, 0x4a,
      0xc8, 0xa2, 0xab, 0xca, 0xe8, 0xb4, 0x34, 0x49, 0x41, 0x8b, 0x20, 0xca,
      0x4b, 0x59, 0x10, 0xc9, 0x1c, 0x03, 0xc0, 0x4a, 0x0f, 0x01, 0x00, 0x00,
      0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  };

  int ret = libusb_control_transfer(
      psvr2_dev, LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_ENDPOINT, 0x9,
      pkt_ep0_0a[0], 0x0, pkt_ep0_0a, sizeof(pkt_ep0_0a), 100);
  ASSERT_MSG(ret >= 0, "USB 0a pkt failed. ret %d\n", ret);

  return 0;
}

/* Dump 5 second of SL data from EP3 */
int dump_sl_ep3_data(libusb_device_handle *psvr2_dev) {
  uint64_t start = get_time_ns();
  unsigned char buf[1024];

  printf("Dumping 5 sec of SL EP3 data\n");

  int ret = libusb_claim_interface(psvr2_dev, 3);
  ASSERT_MSG(ret >= 0, "Could not claim interface 3\n");
  ret = libusb_set_interface_alt_setting(psvr2_dev, 3, 0);
  ASSERT_MSG(ret >= 0, "Could not set interface 3 to alt 0\n");

  do {
    send_ep0_08_pkt(psvr2_dev);
    send_ep0_0a_pkt(psvr2_dev);

    int bytes_read = 0;
    int ret = libusb_bulk_transfer(psvr2_dev, 0x83, buf, sizeof(buf),
                                   &bytes_read, 100);
    ASSERT_MSG(ret >= 0, "Failed to read EP3 data. ret %d\n", ret);
    if (ret == 0) {
      sleep_ms(15);
      continue;
    }

    printf("TS %" PRIu64 "\n", get_time_ns());
    hexdump_buffer("EP3", buf, bytes_read);
  } while (get_time_ns() - start < 5 * TIME_1S_IN_NS);
  return 0;
}

/* Dump 5 second of GS data from EP6 */
int dump_ep6_gs_data(libusb_device_handle *psvr2_dev) {
  uint64_t start = get_time_ns();
  unsigned char buf[512];

  printf("Dumping 5 sec of EP6 GS data\n");

  int ret = libusb_set_interface_alt_setting(psvr2_dev, 5, 1);
  ASSERT_MSG(ret >= 0, "Could not set interface 5 to alt 1\n");
  do {
    int bytes_read = 0;
    int ret = libusb_interrupt_transfer(psvr2_dev, 0x86, buf, sizeof(buf),
                                        &bytes_read, 1000);
    ASSERT_MSG_RET0(ret >= 0, "Failed to read EP6 data. ret %d\n", ret);
    if (ret == 0) {
      sleep_ms(15);
      continue;
    }

    printf("TS %" PRIu64 "\n", get_time_ns());
    hexdump_buffer("EP6 GS", buf, bytes_read);
  } while (get_time_ns() - start < 5 * TIME_1S_IN_NS);

  ret = libusb_set_interface_alt_setting(psvr2_dev, 5, 2);
  ASSERT_MSG(ret >= 0, "Could not set interface 5 to alt 2\n");

  return 0;
}

/* Dump 5 second of VI data from EP7 */
int dump_ep7_cam_data(libusb_device_handle *psvr2_dev) {
  uint64_t start = get_time_ns();
  unsigned char buf[16384];

  printf("Dumping 5 sec of EP7 data\n");

  int ret = libusb_claim_interface(psvr2_dev, 6);
  ASSERT_MSG(ret >= 0, "Could not claim interface 6\n");
  ret = libusb_set_interface_alt_setting(psvr2_dev, 6, 0);
  ASSERT_MSG(ret >= 0, "Could not set interface 6 to alt 0\n");
  do {
    send_ep0_08_pkt(psvr2_dev);
    send_ep0_0a_pkt(psvr2_dev);

    int bytes_read = 0;
    int ret = libusb_bulk_transfer(psvr2_dev, 0x87, buf, sizeof(buf),
                                   &bytes_read, 100);
    ASSERT_MSG_RET0(ret >= 0, "Failed to read EP7 data. ret %d\n", ret);
    if (ret == 0) {
      sleep_ms(15);
      continue;
    }

    printf("TS %" PRIu64 "\n", get_time_ns());
    hexdump_buffer("EP7", buf, bytes_read);
  } while (get_time_ns() - start < 5 * TIME_1S_IN_NS);

  return 0;
}

struct imu_record {
  uint32_t vts;
  int16_t accel[3];
  int16_t gyro[3];
  uint16_t dp_frame_cnt;
  uint16_t dp_line_cnt;
  uint16_t imu_ts;
  uint16_t status;
};

struct imu_usb_record {
  __le32 vts;
  __le16 accel[3];
  __le16 gyro[3];
  __le16 dp_frame_cnt;
  __le16 dp_line_cnt;
  __le16 imu_ts;
  __le16 status;
} __attribute__((packed));

void dump_imu_record(int index, unsigned char *data) {
  struct imu_usb_record in_record;
  memcpy(&in_record, data, sizeof(in_record));
  struct imu_record imu_data;

  imu_data.vts = __le32_to_cpu(in_record.vts);
  for (int i = 0; i < 3; i++) {
    imu_data.accel[i] = __le16_to_cpu(in_record.accel[i]);
    imu_data.gyro[i] = __le16_to_cpu(in_record.gyro[i]);
  }
  imu_data.dp_frame_cnt = __le16_to_cpu(in_record.dp_frame_cnt);
  imu_data.dp_line_cnt = __le16_to_cpu(in_record.dp_line_cnt);
  imu_data.imu_ts = __le16_to_cpu(in_record.imu_ts);
  imu_data.status = __le16_to_cpu(in_record.status);

  printf("Record #%d: TS %u vts %u "
         "accel { %d, %d, %d } gyro { %d, %d, %d } "
         "dp_frame_cnt %u dp_line_cnt %u status %u\n",
         index, imu_data.imu_ts, imu_data.vts, imu_data.accel[0],
         imu_data.accel[1], imu_data.accel[2], imu_data.gyro[0],
         imu_data.gyro[1], imu_data.gyro[2], imu_data.dp_frame_cnt,
         imu_data.dp_line_cnt, imu_data.status);
}

int dump_ep8_imu(libusb_device_handle *psvr2_dev) {
  uint64_t start = get_time_ns();
  unsigned char buf[1024];
  int ret;

  printf("Dumping 5 sec of IMU\n");

  ret = libusb_claim_interface(psvr2_dev, 7);
  ASSERT_MSG(ret >= 0, "Could not claim interface 7\n");
  printf("Setting interface 7 alt mode 1\n");
  ret = libusb_set_interface_alt_setting(psvr2_dev, 7, 1);
  ASSERT_MSG(ret >= 0, "Could not set interface 7 to alt 1\n");

  do {
    send_ep0_08_pkt(psvr2_dev);
    send_ep0_0a_pkt(psvr2_dev);

    int bytes_read = 0;
    int ret = libusb_interrupt_transfer(psvr2_dev, 0x88, buf, sizeof(buf),
                                        &bytes_read, 100);
    ASSERT_MSG_RET0(ret >= 0, "Failed to read IMU data. ret %d\n", ret);

    if (bytes_read < 32) {
      sleep_ms(1);
      continue;
    }

    printf("IMU %u bytes Hdr TS %" PRIu64 "\n", bytes_read, get_time_ns());
    hexdump_buffer("    ", buf, 32);
    for (int pos = 32, i = 0; pos < bytes_read;
         pos += sizeof(struct imu_usb_record), i++) {
      dump_imu_record(i, buf + pos);
    }
  } while (get_time_ns() - start < 5 * TIME_1S_IN_NS);

  return 0;
}

int dump_ep9(libusb_device_handle *psvr2_dev) {
  uint64_t start = get_time_ns();
  unsigned char buf[1024];
  int ret;

  printf("Dumping 5 sec of EP9\n");

  ret = libusb_claim_interface(psvr2_dev, 8);
  ASSERT_MSG(ret >= 0, "Could not claim interface 8\n");
  ret = libusb_set_interface_alt_setting(psvr2_dev, 8, 0);
  ASSERT_MSG(ret >= 0, "Could not set interface 8 to alt 0\n");

  do {
    send_ep0_08_pkt(psvr2_dev);
    send_ep0_0a_pkt(psvr2_dev);

    int bytes_read = 0;
    int ret = libusb_bulk_transfer(psvr2_dev, 0x89, buf, sizeof(buf),
                                   &bytes_read, 100);
    ASSERT_MSG_RET0(ret >= 0, "Failed to read EP9 data. ret %d\n", ret);
    if (ret == 0) {
      sleep_ms(15);
      continue;
    }

    printf("TS %" PRIu64 "\n", get_time_ns());
    hexdump_buffer("EP9", buf, bytes_read);
  } while (get_time_ns() - start < 5 * TIME_1S_IN_NS);

  return 0;
}

/* Dump 5 second of RP data from EP10 */
int dump_ep10_rp_data(libusb_device_handle *psvr2_dev) {
  uint64_t start = get_time_ns();
  unsigned char buf[1024];

  printf("Dumping 5 sec of EP10 data\n");

  int ret = libusb_claim_interface(psvr2_dev, 9);
  ASSERT_MSG(ret >= 0, "Could not claim interface 9\n");
  ret = libusb_set_interface_alt_setting(psvr2_dev, 9, 0);
  ASSERT_MSG(ret >= 0, "Could not set interface 9 to alt 0\n");

  do {
    send_ep0_08_pkt(psvr2_dev);
    send_ep0_0a_pkt(psvr2_dev);

    int bytes_read = 0;
    int ret = libusb_bulk_transfer(psvr2_dev, 0x80 | 10, buf, sizeof(buf),
                                   &bytes_read, 100);
    ASSERT_MSG_RET0(ret >= 0, "Failed to read EP10 data. ret %d\n", ret);
    if (ret == 0) {
      sleep_ms(15);
      continue;
    }

    printf("TS %" PRIu64 "\n", get_time_ns());
    hexdump_buffer("EP10", buf, bytes_read);
  } while (get_time_ns() - start < 5 * TIME_1S_IN_NS);

  return 0;
}

/* Dump 5 second of data from EP11 */
int dump_ep11(libusb_device_handle *psvr2_dev) {
  uint64_t start = get_time_ns();
  unsigned char buf[1024];

  printf("Dumping 5 sec of EP11 data\n");

  int ret = libusb_claim_interface(psvr2_dev, 10);
  ASSERT_MSG(ret >= 0, "Could not claim interface 10\n");
  ret = libusb_set_interface_alt_setting(psvr2_dev, 10, 0);
  ASSERT_MSG(ret >= 0, "Could not set interface 10 to alt 0\n");

  do {
    send_ep0_08_pkt(psvr2_dev);
    send_ep0_0a_pkt(psvr2_dev);

    int bytes_read = 0;
    int ret = libusb_bulk_transfer(psvr2_dev, 0x80 | 11, buf, sizeof(buf),
                                   &bytes_read, 100);
    ASSERT_MSG_RET0(ret >= 0, "Failed to read EP11 data. ret %d\n", ret);
    if (ret == 0) {
      sleep_ms(15);
      continue;
    }

    printf("TS %" PRIu64 "\n", get_time_ns());
    hexdump_buffer("EP11", buf, bytes_read);
  } while (get_time_ns() - start < 5 * TIME_1S_IN_NS);

  return 0;
}

int main(int argc, char **argv) {
  int ret;

  /* Open HID control device */
  hid_device *psvr2_hid = hid_open(PSVR2_VID, PSVR2_PID, NULL);
  ASSERT_MSG(psvr2_hid != NULL, "Could not open PSVR2 HID device. Make sure "
                                "it's switched on, and check permissions\n");

  /* Open raw USB devices */
  libusb_context *ctx;
  libusb_device_handle *psvr2_dev;
  ret = libusb_init(&ctx);
  ASSERT_MSG(ret >= 0, "could not initalize libusb\n");

  psvr2_dev = libusb_open_device_with_vid_pid(ctx, PSVR2_VID, PSVR2_PID);
  ASSERT_MSG(psvr2_dev != NULL, "Could not open PSVR2 device. Make sure it's "
                                "switched on, and check permissions\n");
  printf("Opened PSVR2 device\n");

  ret = libusb_claim_interface(psvr2_dev, 5);
  ASSERT_MSG(ret >= 0, "Could not claim interface 5\n");
  printf("Setting interface 5 alt mode 1\n");
  ret = libusb_set_interface_alt_setting(psvr2_dev, 5, 1);
  ASSERT_MSG(ret >= 0, "Could not set interface 5 to alt 1\n");

  ret = libusb_claim_interface(psvr2_dev, 7);
  ASSERT_MSG(ret >= 0, "Could not claim interface 7\n");
  printf("Setting interface 7 alt mode 1\n");
  ret = libusb_set_interface_alt_setting(psvr2_dev, 7, 1);
  ASSERT_MSG(ret >= 0, "Could not set interface 7 to alt 1\n");

  printf("Initial control packets\n");
  ret = do_initial_controls(psvr2_hid, psvr2_dev);
  ASSERT_MSG(ret >= 0, "Failed initial packet replay\n");
  printf("------------\n");

  ret = dump_sl_ep3_data(psvr2_dev);
  ASSERT_MSG(ret >= 0, "Failed SL EP3 dump\n");
  printf("------------\n");

#if 1
  /*
   * Gaze Setup
   * @todo
   */
  ret = dump_ep6_gs_data(psvr2_dev);
  ASSERT_MSG(ret >= 0, "Failed EP6 dump\n");
  printf("------------\n");
#endif

  /* Camera data feed? */
  dump_ep7_cam_data(psvr2_dev);
  ASSERT_MSG(ret >= 0, "Failed EP7 dump\n");
  printf("------------\n");

#if 1
  ret = dump_ep8_imu(psvr2_dev);
  ASSERT_MSG(ret >= 0, "Failed IMU dump\n");
  printf("------------\n");
#endif

  ret = dump_ep9(psvr2_dev);
  ASSERT_MSG(ret >= 0, "Failed EP9 dump\n");
  printf("------------\n");

  /* RP data feed */
  ret = dump_ep10_rp_data(psvr2_dev);
  ASSERT_MSG(ret >= 0, "Failed EP10 dump\n");
  printf("------------\n");

  ret = dump_ep11(psvr2_dev);
  ASSERT_MSG(ret >= 0, "Failed EP11 dump\n");
  printf("------------\n");

  libusb_close(psvr2_dev);
  libusb_exit(ctx);

  hid_close(psvr2_hid);

  return 0;
}
